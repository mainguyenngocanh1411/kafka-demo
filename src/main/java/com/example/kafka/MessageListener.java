package com.example.kafka;



import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import java.util.concurrent.CountDownLatch;

public class MessageListener {

  public CountDownLatch latch = new CountDownLatch(2);

  public CountDownLatch partitionLatch = new CountDownLatch(2);
  @KafkaListener(topics = "test", groupId = "Listeners", containerFactory = "listenersKafkaListenerContainerFactory")
  public void listenGroup(String message) {
    System.out.println("Received Message in group 'Listeners': " + message);
    latch.countDown();
  }
  @KafkaListener(topics = "test", groupId = "Admins", containerFactory = "adminKafkaListenerContainerFactory")
  public void listenGroupAdmin(String message) {
    System.out.println("Received Message in group 'Admins': " + message);
    latch.countDown();
  }
  @KafkaListener(topicPartitions = @TopicPartition(topic = "partition", partitions = { "0", "3" }))
  public void listenToPartition(@Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
    System.out.println("Received Message: " + message + " from partition: " + partition);
    this.partitionLatch.countDown();
  }
}
