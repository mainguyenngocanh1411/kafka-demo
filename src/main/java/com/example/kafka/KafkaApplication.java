package com.example.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.concurrent.TimeUnit;

@SpringBootApplication public class KafkaApplication {

  @Autowired
  public static KafkaTemplate<String, String> kafkaTemplate;

  public static void main(String[] args) throws Exception {

    ConfigurableApplicationContext context = SpringApplication.run(KafkaApplication.class, args);

    MessageProducer producer = context.getBean(MessageProducer.class);
    MessageListener listener = context.getBean(MessageListener.class);
    /*
     * Sending a Hello World message to topic 'baeldung'.
     * Must be recieved by both listeners with group foo
     * and bar with containerFactory fooKafkaListenerContainerFactory
     * and barKafkaListenerContainerFactory respectively.
     * It will also be recieved by the listener with
     * headersKafkaListenerContainerFactory as container factory
     */
    producer.sendMessage("Hello, Nana!");
    listener.latch.await(10, TimeUnit.SECONDS);
    System.out.println("Stage 1 done!");

    /*
     * Sending message to a topic with 5 partition,
     * each message to a different partition. But as per
     * listener configuration, only the messages from
     * partition 0 and 3 will be consumed.
     */
    for (int i = 0; i < 5; i++) {
      System.out.println("Sending message partition " + i);
      producer.sendMessageToPartion("Hello To Partioned Topic!", i);
    }
    listener.partitionLatch.await(10, TimeUnit.SECONDS);
    System.out.println("Stage 2 done!");
    context.close();
  }

  @Bean
  public MessageProducer messageProducer() {
    return new MessageProducer();
  }

  @Bean
  public MessageListener messageListener() {
    return new MessageListener();
  }


}
